from orders.models import Order


def get_items_in_cart(request):
    if request.user.is_authenticated:
        customer = request.user
        order, created = Order.objects.get_or_create(
                                                     customer=customer,
                                                     completed=False,
                                                     )
        # order = customer.history.get(completed=False)
        items_in_cart = order.get_cart_items
    else:
        # items = []
        order = {'get_cart_total': 0, 'get_cart_items': 0}
        items_in_cart = order['get_cart_items']

    return {
        'items_in_cart': items_in_cart,
        'order': order,
    }
