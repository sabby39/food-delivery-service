from django import template
from products.models import Category
# from products.views import ProductListView

register = template.Library()


@register.inclusion_tag('products/chunks/sidebar.html')
def category_list():

    return {
            'categories': Category.objects.all(),
            # 'view': ProductListView(),
    }
