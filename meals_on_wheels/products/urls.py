from django.urls import path
from .views import (
    ProductListView,
    ProductDetailView,
    CategoryView
    )

app_name = 'products'

urlpatterns = [
    path('menu/', ProductListView.as_view(), name='menu'),
    path('<slug:product_slug>/',
         ProductDetailView.as_view(),
         name='product'
         ),
    path('category/<slug:category_slug>/',
         CategoryView.as_view(),
         name='category',
         ),
]
