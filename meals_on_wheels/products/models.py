from django.db import models
from django.db.models.deletion import CASCADE
from django.urls import reverse


class Product(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    overview = models.TextField(
                                max_length=200,
                                blank=True,
                                null=True,
                                )
    description = models.TextField(
                                   max_length=500,
                                   blank=True,
                                   null=True,
                                   )
    # dollar_price = models.DecimalField(
    #                             max_digits=12,
    #                             decimal_places=2,
    #                             blank=True,
    #                             )
    category = models.ManyToManyField(
                                      'Category',
                                      related_name='products',
                                      )
    ingredient = models.ManyToManyField(
                                        'Ingredient',
                                        related_name='products'
                                        )
    cover_image = models.ImageField(blank=True, null=True)
    views = models.IntegerField(default=0, blank=True, null=True)
    date_added = models.DateTimeField(
                                      auto_now_add=True,
                                      blank=True,
                                      null=True,
                                      )
    stripe_product_id = models.CharField(
                                         max_length=100,
                                         blank=True,
                                         null=True,
                                         )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date_added', '-views', 'name']

    @property
    def imageURL(self):
        try:
            url = self.cover_image.url
        except ValueError:
            url = ''
        return url

    @property
    def get_absolute_url(self):
        return reverse(
                       'products:product',
                       kwargs={'product_slug': self.slug},
                       )


class Price(models.Model):
    product = models.ForeignKey(
                                Product,
                                on_delete=CASCADE,
                                related_name='price'
                                )
    stripe_price_id = models.CharField(max_length=100)
    cent_price = models.IntegerField(default=0)

    def get_display_price(self):
        return f"{self.cent_price / 100:.2f}"


class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    ordinal = models.PositiveSmallIntegerField(
                                               blank=True,
                                               null=True,
                                               )

    class Meta:
        verbose_name_plural = 'categories'
        ordering = ['ordinal']

    def __str__(self):
        return self.name

    @property
    def get_absolute_url(self):
        return reverse(
                       'products:category',
                       kwargs={'category_slug': self.slug},
                       )


class Ingredient(models.Model):
    name = models.CharField(max_length=100)
    priority = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-priority']


class Photo(models.Model):
    image = models.ImageField(blank=True, null=True)
    product = models.ForeignKey(
                                Product,
                                on_delete=CASCADE,
                                related_name='photos'
                                )

    def __str__(self):
        try:
            url = self.image.url
        except ValueError:
            url = ''
        return url
