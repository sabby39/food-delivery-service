from django.views.generic.list import ListView
from django.views.generic import DetailView
from .models import (
                     Product,
                     Price,
                     Category,
                     Ingredient,
                     Photo,
                     )


class ProductListView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'products/menu.html'

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        # context['price'] = self.object.price.all()[0].get_display_price()
        context['categories'] = Category.objects.all()
        return context


class CategoryView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'products/menu.html'

    def get_queryset(self):
        return super().get_queryset().filter(
            category__slug=self.kwargs['category_slug']
            )

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class ProductDetailView(DetailView):
    model = Product
    template_name = 'products/product.html'
    slug_url_kwarg = 'product_slug'
    context_object_name = 'product'

    def get(self, request, product_slug):
        response = super().get(request)
        self.object.views += 1
        self.object.save()
        return response

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        print(self.object.price.all()[0].get_display_price())
        price = Price.objects.get(product=self.object)
        print(price)
        context['price'] = price
        context['ingredients'] = Ingredient.objects.filter(
                                 products=self.get_object()
                                                           )
        any_photos = Photo.objects.filter(
                                          product=self.get_object()
                                          )
        if any_photos:
            context['any_photos'] = any_photos
            context['first_photo'] = any_photos[0]
            context['remaining_photos'] = any_photos[1:]
        context['categories'] = Category.objects.filter(
                                products=self.get_object()
                                                        )
        return context
