from django.contrib import admin
from .models import (
                     Product,
                     Price,
                     Category,
                     Ingredient,
                     Photo,
                     )


class PhotoInlineAdmin(admin.TabularInline):
    model = Photo
    extra = 0


class PriceInlineAdmin(admin.TabularInline):
    model = Price
    extra = 0


class ProductAdmin(admin.ModelAdmin):
    inlines = (PhotoInlineAdmin, PriceInlineAdmin)
    list_display = ('id', 'name', 'slug', 'views', 'date_added')
    list_display_links = ('id', 'name')
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name', 'category__name',)
    list_filter = ('category',)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    list_display_links = ('id', 'name')
    prepopulated_fields = {'slug': ('name',)}


class IngredientAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    list_editable = ('name',)


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Ingredient, IngredientAdmin)
admin.site.register(Photo)
