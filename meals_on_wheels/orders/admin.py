from django.contrib import admin
from .models import Order, ItemInCart


class OrderAdmin(admin.ModelAdmin):
    list_display = (
                    'id',
                    'total',
                    'opened_on',
                    'closed_on',
                    'payment_option',
                    'completed'
                    )
    list_display_links = ('id', )
    search_fields = ('customer__username', 'payment_option', )
    list_filter = ('completed', )
    readonly_fields = ('total', 'opened_on', 'closed_on',)


admin.site.register(Order, OrderAdmin)
admin.site.register(ItemInCart)
