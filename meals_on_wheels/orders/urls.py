from django.urls import path

from .views import (
                    CartView,
                    CheckoutView,
                    UpdateItemView,
                    ThankYouView,
                    DispatchPaymentView,
                    ProcessStripePayment,
                    CreateStripeSessionView,
                    SuccessStripeView,
                    CancelStripeView,
                    ProcessBonusPayment,
                    EmptyCartView,
                    )

app_name = 'orders'

urlpatterns = [
    path(
         'cart/',
         CartView.as_view(),
         name='cart'
         ),
    path(
         'update_item/',
         UpdateItemView.as_view(),
         name='update_item'
         ),
    path(
         'checkout/',
         CheckoutView.as_view(),
         name='checkout'
         ),
    path(
         'dispatch_payment/',
         DispatchPaymentView.as_view(),
         name='dispatch_payment'
         ),
    path(
         'bonus_payment/',
         ProcessBonusPayment.as_view(),
         name='bonus_payment'
         ),
    path(
         'stripe_payment/',
         ProcessStripePayment.as_view(),
         name='stripe_payment'
         ),
    path(
         'create-stripe-session/',
         CreateStripeSessionView.as_view(),
         name='create-stripe-session'
         ),
    path(
         'success/',
         SuccessStripeView.as_view(),
         name='success'
         ),
    path(
         'cancel/',
         CancelStripeView.as_view(),
         name='cancel'
         ),
    path(
         'thank_you/',
         ThankYouView.as_view(),
         name='thank_you'
         ),
    path(
         'empty-cart/',
         EmptyCartView.as_view(),
         name='empty_cart'
         ),
]
