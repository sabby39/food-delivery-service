from http.client import HTTPResponse
import json
from django.http.response import (
                                  HttpResponseRedirect,
                                  JsonResponse,
                                  )
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import (
                     Order,
                     ItemInCart,
                     )
from .forms import CheckoutForm
from products.models import (
                             Product,
                             Price,
                             )
from django.contrib.auth import get_user_model
from django.shortcuts import (
                              get_object_or_404,
                              redirect,
                              )
from django.urls import (
                         reverse,
                         reverse_lazy,
                         )
from decimal import Decimal
from django.conf import settings
import stripe


stripe.api_key = settings.STRIPE_SECRET_KEY


class CartView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('customers:login')
    model = ItemInCart
    context_object_name = 'items'
    template_name = 'orders/cart.html'

    def get_queryset(self):
        if self.request.user.is_authenticated:
            customer = self.request.user
            order, created = Order.objects.get_or_create(
                                                         customer=customer,
                                                         completed=False
                                                        )
            return order.items.all()
        else:
            return []


class CheckoutView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('customers:login')
    model = ItemInCart
    context_object_name = 'items'
    template_name = 'orders/checkout.html'

    def get(self, request, *args, **kwargs):
        customer = self.request.user
        order, created = Order.objects.get_or_create(
                                                     customer=customer,
                                                     completed=False,
                                                     )
        if not order.items.all():
            return HttpResponseRedirect(reverse('orders:empty_cart'))

        return super().get(self, request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.is_authenticated:
            customer = self.request.user
            order, created = Order.objects.get_or_create(
                             customer=customer, completed=False
                                                        )
            return order.items.all()
        else:
            return []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            customer = self.request.user
            order, created = Order.objects.get_or_create(
                             customer=customer, completed=False
                                                        )
        else:
            order = {'get_cart_total': 0, 'get_cart_items': 0}
        context['order'] = order

        customer = get_object_or_404(
                                get_user_model(),
                                pk=self.request.user.pk
                                )
        checkout_form = CheckoutForm(instance=customer)
        context['checkout_form'] = checkout_form
        # getcontext().prec = 2
        new_bonuses = order.get_cart_total * Decimal(0.01)
        customer.bonus_account += new_bonuses
        context['new_bonuses'] = f"{new_bonuses:.2f}"
        return context


class DispatchPaymentView(FormView):
    form_class = CheckoutForm

    def form_valid(self, form):
        if not self.request.user.is_authenticated:
            return HTTPResponse(
                'Sorry, it looks like you are not logged in :('
                )

        customer = self.request.user
        order, created = Order.objects.get_or_create(
                                                     customer=customer,
                                                     completed=False,
                                                     )
        order.comment = form.cleaned_data['comment']

        if not order.items.all():
            return HttpResponseRedirect(reverse('orders:empty_cart'))

        if form.cleaned_data['payment_option'] == 'Cash':
            order.payment_option = form.cleaned_data['payment_option']
            order.total = order.get_cart_total
            customer.bonus_account += (order.total * Decimal(0.01))
            order.completed = True
            order.save()
            customer.save()
            return HttpResponseRedirect(reverse('orders:thank_you'))

        elif form.cleaned_data['payment_option'] == 'Card':
            order.payment_option = 'Card'
            order.save()
            return HttpResponseRedirect(
                reverse('orders:create-stripe-session')
                )
        elif form.cleaned_data['payment_option'] == 'Bonuses':
            order.payment_option = 'Bonuses'
            order.save()
            return HttpResponseRedirect(reverse('orders:bonus_payment'))


class ProcessStripePayment(TemplateView):
    template_name = 'orders/stripe_payment.html'


class CreateStripeSessionView(View):
    def get(self, request, *args, **kwargs):
        # DOMAIN = 'http://127.0.0.1:8000'     # !! DEVELOPMENT
        DOMAIN = 'http://45.94.157.160:999'  # !! PRODUCTION
        customer = self.request.user
        order, created = Order.objects.get_or_create(
                                                     customer=customer,
                                                     completed=False,
                                                     )
        items = order.items.all()
        checkout_session = stripe.checkout.Session.create(
            line_items=[
                        {
                         'price': Price.objects.get(
                                                    product=item.product
                                                    ).stripe_price_id,
                         'quantity': item.quantity,
                         } for item in items
                         ],
            mode='payment',
            success_url=f"{DOMAIN}/orders/success/",
            cancel_url=f"{DOMAIN}/orders/cancel/",
        )
        return redirect(checkout_session.url)


class SuccessStripeView(View):

    def get(self, request):
        customer = request.user
        order = customer.history.get(completed=False)
        order.total = order.get_cart_total
        customer.bonus_account += (order.total * Decimal(0.01))
        customer.save()
        order.completed = True
        order.save()
        return HttpResponseRedirect(reverse('orders:thank_you'))


class CancelStripeView(TemplateView):
    template_name = 'orders/cancel.html'


class ProcessBonusPayment(TemplateView):
    template_name = 'orders/bonus_payment.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customer = self.request.user
        bonuses = customer.bonus_account
        context['bonuses'] = bonuses

        order, created = Order.objects.get_or_create(
                            customer=customer, completed=False
                                                     )
        context['due_amount'] = order.get_cart_total
        context['sufficient'] = bonuses >= order.get_cart_total

        return context

    def post(self, request):
        customer = self.request.user
        order, created = Order.objects.get_or_create(
                        customer=customer, completed=False
                                                     )
        order.total = order.get_cart_total
        customer.bonus_account -= order.total
        order.completed = True
        order.save()
        customer.save()

        return HttpResponseRedirect(reverse('orders:thank_you'))


class UpdateItemView(View):
    def post(self, request):
        data = json.loads(request.body)
        productID = data['productID']
        action = data['action']

        customer = self.request.user
        product = Product.objects.get(id=productID)
        order, created = Order.objects.get_or_create(
                             customer=customer, completed=False
                                                        )

        item_in_cart, created = ItemInCart.objects.get_or_create(
                                                   order=order,
                                                   product=product
                                                   )

        if action == 'add':
            item_in_cart.quantity = item_in_cart.quantity + 1
        elif action == 'remove':
            item_in_cart.quantity = item_in_cart.quantity - 1

        if item_in_cart.quantity > 0:
            item_in_cart.save()
        else:
            item_in_cart.delete()

        return JsonResponse('Item was added', safe=False)


class ThankYouView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'orders/thank_you.html'

    def get_queryset(self):
        return super().get_queryset().order_by('-views')[:5]


class EmptyCartView(TemplateView):
    template_name = 'orders/empty_cart.html'
