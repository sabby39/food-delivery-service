from django import forms
from django.contrib.auth import get_user_model
from django.forms import (
                          ModelForm,
                          Textarea,
                          TextInput,
                          )


class CheckoutForm(ModelForm):

    CHOICES = [
               ('Card', 'Credit Card (via Stripe) 💳'),
               ('Cash', 'Cash on Delivery 💵'),
               ('Bonuses', 'Pay with Bonuses 🔃'),
               ]
    payment_option = forms.ChoiceField(
                                       choices=CHOICES,
                                       widget=forms.RadioSelect()
                                       )

    comment = forms.CharField(
                              widget=Textarea(
                                  attrs={
                                         'rows': 5,
                                         'cols': 40,
                                         'class': 'form-control',
                                         'type': 'text',
                                         'name': 'comment',
                                         'placeholder': 'Comment...',
                                         }
                                             ),
                              required=False
                              )

    class Meta:
        model = get_user_model()
        fields = [
                  'first_name',
                  'last_name',
                  'phone',
                  'city',
                  'street_address'
                  ]

        widgets = {
            'first_name': TextInput(
                attrs={
                       'class': 'form-control',
                       'type': 'text',
                       'name': 'first_name',
                       'placeholder': 'First Name...',
                       }
                               ),
            'last_name': TextInput(
                attrs={
                       'class': 'form-control',
                       'type': 'text',
                       'name': 'last_name',
                       'placeholder': 'Last Name...',
                       }
                               ),
            'phone': TextInput(
                attrs={
                       'class': 'form-control',
                       'type': 'text',
                       'name': 'phone',
                       'placeholder': 'Phone...',
                       }
                               ),
            'city': TextInput(
                attrs={
                       'class': 'form-control',
                       'type': 'text',
                       'name': 'city',
                       'placeholder': 'City...',
                       }
                               ),
            'street_address': TextInput(
                attrs={
                       'class': 'form-control',
                       'type': 'text',
                       'name': 'street_address',
                       'placeholder': 'Street Address...',
                       }
                               ),
        }
