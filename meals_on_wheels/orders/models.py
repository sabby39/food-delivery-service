from django.db import models
from django.db.models.deletion import DO_NOTHING, SET_NULL
from products.models import Product
from customers.models import Customer
from decimal import Decimal


class Order(models.Model):
    customer = models.ForeignKey(
                                 Customer,
                                 on_delete=DO_NOTHING,
                                 related_name='history',
                                 blank=True,
                                 null=True,
                                 )
    total = models.DecimalField(
                                max_digits=12,
                                decimal_places=2,
                                blank=True,
                                null=True,
                                )
    payment_option = models.CharField(
                                      max_length=100,
                                      blank=True,
                                      null=True,
                                      )
    opened_on = models.DateTimeField(auto_now_add=True)
    closed_on = models.DateTimeField(auto_now=True)
    completed = models.BooleanField(default=False)
    comment = models.TextField(
                               blank=True,
                               null=True,
                               )

    def __str__(self):
        return f'Order #{self.id}'

    @property
    def get_cart_total(self):
        return sum(item.get_subtotal for item in self.items.all())

    @property
    def get_cart_items(self):
        return sum(item.quantity for item in self.items.all())


class ItemInCart(models.Model):
    product = models.ForeignKey(
                                Product,
                                on_delete=SET_NULL,
                                blank=True,
                                null=True,
                                )

    order = models.ForeignKey(
                              Order,
                              on_delete=SET_NULL,
                              blank=True,
                              null=True,
                              related_name='items'
                              )
    quantity = models.PositiveSmallIntegerField(default=0, blank=True)

    time_added = models.DateTimeField(
                                      auto_now_add=True,
                                      blank=True,
                                      null=True,
                                      )

    def __str__(self):
        return self.product.name

    class Meta:
        verbose_name = 'items in cart'
        ordering = ['-time_added', ]

    @property
    def get_subtotal(self):
        return (
                Decimal(
                        self.product.price.all()[0].cent_price
                        ) / 100
                ) * self.quantity
