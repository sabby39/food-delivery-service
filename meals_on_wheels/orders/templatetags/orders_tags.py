from django import template
# from orders.models import Order, ItemInCart


register = template.Library()


@register.filter
def items_in_order(items, order):
    return items.filter(order=order)
