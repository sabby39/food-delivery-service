from django.urls import path
from django.contrib.auth.views import LogoutView
from .views import (
     LoginCustomerView,
     ProfileDetailsView,
     ProfileEditView,
     RegisterView,
     CustomPasswordChangeView,
     HistoryView,
)

app_name = 'customers'

urlpatterns = [
    path('login/',
         LoginCustomerView.as_view(),
         name='login'
         ),
    path('logout/',
         LogoutView.as_view(),
         name='logout'
         ),
    path('register/',
         RegisterView.as_view(),
         name='register'
         ),
    path('profile-details/',
         ProfileDetailsView.as_view(),
         name='profile-details'
         ),
    path('profile-edit/',
         ProfileEditView.as_view(),
         name='profile-edit'
         ),
    path('password-change/',
         CustomPasswordChangeView.as_view(),
         name='password-change'
         ),
    path('history/', HistoryView.as_view(), name='history'),
]
