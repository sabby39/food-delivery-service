from django.db import models
from django.contrib.auth.models import AbstractUser


class Customer(AbstractUser):
    phone = models.CharField(max_length=20, blank=True)
    city = models.CharField(max_length=225, blank=True)
    street_address = models.CharField(max_length=225, blank=True)
    bonus_account = models.DecimalField(
                                        default=0,
                                        max_digits=12,
                                        decimal_places=2,
                                        )

    def __str__(self) -> str:
        return self.first_name or self.username

    @property
    def get_full_name(self) -> str:
        if self.first_name is None or self.last_name is None:
            return self.username
        else:
            return f'{self.first_name} {self.last_name}'
