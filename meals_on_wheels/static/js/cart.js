const updateButtons = document.querySelectorAll('.update-cart')
// console.log(updateButtons)

updateButtons.forEach((button) => {
    button.addEventListener('click', function(button) {
        const productID = this.dataset.product;
        const action = this.dataset.action;
        // console.log(`productID: ${productID}, action: ${action}`);

        // console.log(`USER: ${user}`);
        if (user == 'AnonymousUser') {
            console.log('Not logged in');
        } else {
            updateUserOrder(productID, action);
        }
    })
})

function updateUserOrder(productID, action) {
    console.log(`Host: ${window.location.hostname}, port: ${window.location.port}`)
    console.log('User is logged in, sending data...')

    const url = `http://${window.location.hostname}:${window.location.port}/orders/update_item/`
    console.log(`URL: ${url}`)

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken,
        },
        body: JSON.stringify({'productID': productID, 'action': action}),
    })
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(`data: ${data}`);
        location.reload();
    })
}
